<?php

namespace app\controller;

use app\BaseController;
use app\service\Atomic;
use Swoole\Server;
use think\App;

class Index extends BaseController
{
    public function index(App $app, Atomic $atomic,Server $server)
    {
        if ($this->request->param('to')) {
            //接口投递消息
            $event = event('swoole.websocket.Point', ['api' => true, 'uid' => $this->request->param('uid'), 'to' => $this->request->param('to'), 'content' => $this->request->param('msg')]);
            dump($event);
        }
        if ($this->request->param('app')) {
            dump($app);
        }
        if ($this->request->param('vote')) {
            $atomic->add((string)rand(1, 5));
            $atomic->add((string)rand(1, 5), 2);
            $atomic->add((string)rand(1, 5), rand(1, 10));
            dump($atomic->all());
        }
        if ($this->request->param('task')) {
            $server->task(['test'=>'test value']);
        }
    }
}
