<?php
return [
    'test' => [
        //队列连接-需要返回一个队列连接
        'conn' => [
            //类名
            'class' => \app\service\queue\test\Conn::class,
            //方法名
            'method' => 'getConn'
        ],
        //无消息休息秒数
        'sleep' => 1,
        //队列消费命令
        'exec' => 'blPop',
        //消费命令等待时间
        'timeout' => 2,
        //消费线程数
        'pop_num' => 1,
        //进程名称
        'process_name' => 'queue test',
        //队列key
        'topics' => 'queue',
        //业务逻辑
        'logic' => [
            //类名
            'class' => \app\service\queue\test\Logic::class,
            //调用方法关键字,一级类目
            'call_func_key' => 'method'
        ],
        'err_break' => false
    ]
];