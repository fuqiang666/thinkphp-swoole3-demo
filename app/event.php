<?php


// 事件定义文件
return [
    'bind' => [
    ],

    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'swoole.task' => ['\app\listener\SwooleTask'],
        'swoole.finish' => ['\app\listener\SwooleTaskFinish'],
        'swoole.init' => ['\app\listener\SwooleInit'],
    ],

    'subscribe' => [
        '\app\subscribe\Timer'
    ],
];
