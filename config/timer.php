<?php
return [
    [
        //执行周期(毫秒)
        'tally' => 2000,
        //事件名称-注意大小写
        'event' => 'Timer',
        //是否等待事件-事件业务完成后开始周期计算
        'wait' => true
    ]
];