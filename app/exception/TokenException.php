<?php
declare (strict_types = 1);

namespace app\exception;


class TokenException extends \RuntimeException
{
    protected $error;

    public function __construct($error, $code = 0)
    {
        $this->error   = $error;
        $this->message = $error;
        $this->code    = $code;
    }

    /**
     * 获取验证错误信息
     * @access public
     * @return array|string
     */
    public function getError()
    {
        return $this->error;
    }
}