<?php

namespace app;

use AlibabaCloud\Tea\Exception\TeaUnableRetryError;
use app\exception\TokenException;
use think\exception\Handle;
use think\exception\ValidateException;
use think\facade\Log;
use think\Response;
use Throwable;

/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        if ($exception instanceof ValidateException || $exception instanceof TokenException)
            return;

        $log = [
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'trace' => $this->traces($exception)
        ];
        Log::channel('err')->write(json_encode($log, 320), 'error');
        // 使用内置的方式记录异常日志
//        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 其他错误交给系统处理
//        if ($this->app->isDebug())
//            return parent::render($request, $e);
        // 添加自定义异常处理机制
        if ($e instanceof ValidateException)
            return json(['msg' => $e->getMessage(), 'code' => 422, 'data' => []]);
        if ($e instanceof TokenException) {
            return json(['msg' => $e->getMessage(), 'code' => 401, 'data' => []]);
        }
        if ($e instanceof TeaUnableRetryError) {
            return json(['msg' => $e->getMessage(), 'code' => 403, 'data' => $e->getLastRequest()]);
        }
        return json(['msg' => $e->getMessage(), 'code' => 500, 'data' => []]);
    }

    public function traces(Throwable $exception)
    {
        $traces = [];
        $nextException = $exception;
        do {
            $traces[] = [
                'name' => get_class($nextException),
                'file' => $nextException->getFile(),
                'line' => $nextException->getLine(),
                'code' => $this->getCode($nextException),
                'message' => $this->getMessage($nextException),
                'trace' => $nextException->getTrace(),
                'source' => $this->getSourceCode($nextException),
            ];
        } while ($nextException = $nextException->getPrevious());
        return $traces;
    }
}
