<?php


namespace app\lib;

use Endroid\QrCode\QrCode as FactoryQrCode;

class QrCode
{

    public function Base64(string $s)
    {
        $qrCode = new FactoryQrCode($s);
        $qrCode->setWriterByName('png');
        $qrCode->setEncoding('UTF-8');
//        $qrCode->write
        return $qrCode;
    }
}