<?php


namespace app\service\queue\test;



class Conn
{
    public static function getConn()
    {
        $conn = new \Redis();
        $conn->pconnect('127.0.0.1', 6379);
//        $conn->auth('');
//        $conn->setOption(\Redis::OPT_READ_TIMEOUT, -1);
//        $conn->select(15);
        return $conn;
    }
}