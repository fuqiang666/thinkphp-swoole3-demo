<?php


namespace app\subscribe;


//定时任务事件监听
class Timer
{
    /**
     * 命名规范是on+事件标识,所以该方法的事件名称为event('Timer')
     */
    public function onTimer()
    {
        var_dump(time());
    }
}