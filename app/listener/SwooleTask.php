<?php
declare (strict_types=1);

namespace app\listener;



use Swoole\Server\Task;

class SwooleTask
{
    /**
     * 事件监听处理
     *
     * @return mixed
     */
    public function handle(Task $task)
    {
        var_dump('on task');
        var_dump($task->data);
        $task->finish($task->data);
        return ;
    }
}
